<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<li onclick="init_task_modal(<?php echo $task['id']; ?>);return false;" data-task-id="<?php echo $task['id']; ?>" class="task<?php if($task['current_user_is_assigned']){echo ' current-user-task';} if((!empty($task['duedate']) && $task['duedate'] < date('Y-m-d')) && $task['status'] != Tasks_model::STATUS_COMPLETE){ echo ' overdue-task'; } ?><?php if(!$task['current_user_is_assigned'] && $task['current_user_is_creator'] == '0' && !is_admin()){echo ' not-sortable';} ?> kanban-item"
  data-border="<?php $dd = $task['duedate']; $td = date("Y-m-d");
    if ($dd == $td) { echo "warning"; }
    else if ($dd > $td) { echo "success"; }
    else { echo "danger"; }
  ?>">
  <?php echo $task['name']; ?>
  <div class="kanban-footer d-flex justify-content-between mt-1">
    <div class="kanban-footer-left d-flex">
      <div class="kanban-due-date d-flex align-items-center mr-50"><i class="bx bx-time-five font-size-small mr-25"></i><span class="font-size-small"><?php echo $task['duedate']; ?></span></div>
      <?php if($task['total_checklist_items'] > 0){ ?>
        <div class="kanban-tasks d-flex align-items-center mr-50"><i class="bx bx-task-alt font-size-small mr-25"></i><span class="font-size-small"
          ><?php echo $task['total_finished_checklist_items']; ?>
          /
          <?php echo $task['total_checklist_items']; ?></span></div>
      <?php } ?>
      <div class="kanban-comment d-flex align-items-center mr-50" data-toggle="tooltip" data-title="<?php echo _l('task_comments'); ?>"><i class="bx bx-message font-size-small mr-25"></i><span class="font-size-small"><?php echo $task['total_comments']; ?></span></div>
      <div class="kanban-attachment d-flex align-items-center" data-toggle="tooltip" data-title="<?php echo _l('task_view_attachments'); ?>"><i class="bx bx-link-alt font-size-small mr-25"></i><span class="font-size-small"><?php echo $task['total_files']; ?></span></div>
    </div>
    <div class="kanban-footer-right">
      <div class="kanban-users">
        <ul class="list-unstyled users-list m-0 d-flex align-items-center">
       <?php
       echo format_members_by_ids_and_names_kanban($task['assignees_ids'],$task['assignees'],false,'staff-profile-image-xs');
       ?>
        </ul>
      </div>
    </div>
  </div>
  <?php $tags = get_tags_in($task['id'],'task');
   if(count($tags) > 0){ ?>
    <div class="mtop5 kanban-tags">
      <?php echo render_tags($tags); ?>
    </div>
  <?php } ?>
</li>
