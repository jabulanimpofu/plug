<?php defined('BASEPATH') or exit('No direct script access allowed');

$where = array();
if($this->input->get('project_id')){
  $where['rel_id'] = $this->input->get('project_id');
  $where['rel_type'] = 'project';
}
foreach ($task_statuses as $index=>$status) {
  $total_pages = ceil($this->tasks_model->do_kanban_query($status['id'],$this->input->get('search'),1,true,$where)/get_option('tasks_kanban_limit'));
  ?>
  <ul class="kan-ban-col tasks-kanban kanban-board"  data-id="kanban-board-<?php echo $index; ?>" data-order="<?php echo $index; ?>" style="width: 250px; margin-left: 15px; margin-right: 15px;" data-col-status-id="<?php echo $status['id']; ?>" data-total-pages="<?php echo $total_pages; ?>">
    <li class="kan-ban-col-wrapper">
    <header class="kanban-board-header">
			<div class="kanban-title-board"><?php echo format_task_status($status['id'],false,true); ?></div>
		</header>
        <div class="kan-ban-content-wrapper">
            <ul class="status tasks-status sortable relative kanban-drag" data-task-status-id="<?php echo $status['id']; ?>">
              <?php
              $tasks = $this->tasks_model->do_kanban_query($status['id'],$this->input->get('search'),1,false,$where);
              $total_tasks = count($tasks);
              foreach ($tasks as $task) {
                if ($task['status'] == $status['id']) {
                  $this->load->view('admin/tasks/_kan_ban_card',array('task'=>$task,'status'=>$status['id']));
                } } ?>
                <?php if($total_tasks > 0 ){ ?>
                <li class="text-center not-sortable kanban-load-more" data-load-status="<?php echo $status['id']; ?>">
                 <a href="#" class="btn btn-default btn-block<?php if($total_pages <= 1){echo ' disabled';} ?>" data-page="1" onclick="kanban_load_more(<?php echo $status['id']; ?>,this,'tasks/tasks_kanban_load_more',265,360); return false;";><?php echo _l('load_more'); ?></a>
               </li>
               <?php } ?>
               <li class="text-center not-sortable mtop30 kanban-empty<?php if($total_tasks > 0){echo ' hide';} ?>">

              </li>
            </ul>
        </div>
      </li>
    </ul>
      <?php } ?>
