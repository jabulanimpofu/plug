<?php defined('BASEPATH') or exit('No direct script access allowed');
   $totalQuickActionsRemoved = 0;
   $quickActions = $this->app->get_quick_actions_links();
   foreach($quickActions as $key => $item){
    if(isset($item['permission'])){
     if(!has_permission($item['permission'],'','create')){
       $totalQuickActionsRemoved++;
     }
   }
   }
   ?>

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand"
                        href="../../../html/ltr/vertical-menu-template/index.html">
                        <div class="brand-logo"><img class="logo" src="<?php base_url('assets/app-assets/images/logo/logo.png'); ?>" />
                        </div>
                        <h2 class="brand-text mb-0">Frest</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                            class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i
                            class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary"
                            data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation"
                data-icon-style="lines">
                <?php if($totalQuickActionsRemoved != count($quickActions)){ ?>
                <li class="quick-links  nav-item"><a href="#"><i
                            class="menu-livicon" data-icon="desktop"></i><span class="menu-title"
                            data-i18n="Quick links">Quick links</span><span
                            class="badge badge-light-danger badge-pill badge-round float-right mr-2"><?php echo count($quickActions);?></span></a>
                    <ul class="menu-content">
                    <?php
                  foreach($quickActions as $key => $item){
                   $url = '';
                   if(isset($item['permission'])){
                     if(!has_permission($item['permission'],'','create')){
                      continue;
                    }
                  }
                  if(isset($item['custom_url'])){
                    $url = $item['url'];
                  } else {
                    $url = admin_url(''.$item['url']);
                  }
                  $href_attributes = '';
                  if(isset($item['href_attributes'])){
                    foreach ($item['href_attributes'] as $key => $val) {
                      $href_attributes .= $key . '=' . '"' . $val . '"';
                    }
                  }
                  ?>
                        <li><a href="<?php echo $url; ?>" <?php echo $href_attributes; ?>><i class="bx bx-right-arrow-alt"></i><span
                                    class="menu-item" data-i18n="<?php echo $item['name']; ?>"><?php echo $item['name']; ?></span></a>
                        </li>
                     <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <li class=" navigation-header"><span>Menu</span>
                </li>
                
                <?php foreach($sidebar_menu as $key => $item){
         if(isset($item['collapse']) && count($item['children']) === 0) {
           continue;
         }
         ?>
                <li class=" nav-item menu-item-<?php echo $item['slug']; ?>" <?php echo _attributes_to_string(isset($item['li_attributes']) ? $item['li_attributes'] : []); ?>><a
                  href="<?php echo count($item['children']) > 0 ? '#' : $item['href']; ?>"
                  <?php echo _attributes_to_string(isset($item['href_attributes']) ? $item['href_attributes'] : []); ?>
                  ><i class="menu-livicon" data-icon="retweet"></i><span
                            class="menu-title" data-i18n="<?php echo _l($item['name'],'', false); ?>"><?php echo _l($item['name'],'', false); ?></span></a>
                            <?php if(count($item['children']) > 0){ ?>
                    <ul class="menu-content">
                    <?php foreach($item['children'] as $submenu){ ?>
                        <li class="sub-menu-item-<?php echo $submenu['slug']; ?>"
                        <?php echo _attributes_to_string(isset($submenu['li_attributes']) ? $submenu['li_attributes'] : []); ?>><a href="<?php echo $submenu['href']; ?>"
                        <?php echo _attributes_to_string(isset($submenu['href_attributes']) ? $submenu['href_attributes'] : []); ?>
                        ><i class="bx bx-right-arrow-alt"></i><span class="menu-item"
                                    data-i18n="<?php echo _l($submenu['name'],'',false); ?>"><?php echo _l($submenu['name'],'',false); ?></span></a>
                        </li>
                        </li>
            <?php } ?>
                    </ul>
            <?php } ?>
                </li>
      <?php hooks()->do_action('after_render_single_aside_menu', $item); ?>
      <?php } ?>
      <?php hooks()->do_action('after_render_aside_menu'); ?>
      <?php $this->load->view('admin/projects/pinned'); ?>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
