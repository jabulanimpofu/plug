<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- Earnings Widget Swiper Starts -->
<div class="widget home-activity" id="widget-<?php echo basename(__FILE__,".php"); ?>" data-name="<?php echo _l('user_widget'); ?>">
   <div class="card  user-data">
      <div class="card-content">
         <div class="card-body py-1">
            <div class="widget-dragger"></div>
               <!-- earnings swiper starts -->
            <div class="horizontal-scrollable-tabs">
               <div class="horizontal-tabs">
                  <ul class="nav nav-tabs nav-tabs-horizontal" role="tablist">
                     <li class="nav-item current">
                        <a class="nav-link active" id="tasks-tab" data-toggle="tab" href="#tasks" aria-controls="tasks" role="tab" aria-selected="true">
                           <i class="bx bx-task align-middle"></i>
                           <span class="align-middle"><?php echo _l('home_my_tasks'); ?></span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link " id="projects-tab" data-toggle="tab" href="#projects" aria-controls="projects" role="tab" aria-selected="true" onclick="init_table_staff_projects(true);">
                           <i class="bx bx-bar-chart align-middle"></i>
                           <span class="align-middle"><?php echo _l('home_my_projects'); ?></span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link " id="reminders-tab" data-toggle="tab" href="#reminders" aria-controls="reminders" role="tab" aria-selected="true" onclick="initDataTable('.table-my-reminders', admin_url + 'misc/my_reminders', undefined, undefined,undefined,[2,'asc']);">
                           <i class="bx bx-time align-middle"></i>
                           <span class="align-middle"><?php echo _l('my_reminders'); ?></span>
                        </a>
                     </li>
                     <?php if((get_option('access_tickets_to_none_staff_members') == 1 && !is_staff_member()) || is_staff_member()){ ?>
                     <li class="nav-item">
                        <a class="nav-link " id="tickets-tab" data-toggle="tab" href="#tickets" aria-controls="tickets" role="tab" aria-selected="true" onclick="init_table_tickets(true);">
                           <i class="bx bx-flag align-middle"></i>
                           <span class="align-middle"><?php echo _l('home_tickets'); ?></span>
                        </a>
                     </li>
                     <?php } ?>
                  
                     <?php if(is_staff_member()){ ?>
                     <li class="nav-item">
                        <a class="nav-link " id="announcements-tab" data-toggle="tab" href="#announcements" aria-controls="announcements" role="tab" aria-selected="true" onclick="init_table_announcements(true);">
                           <i class="bx bx-megaphone align-middle"></i>
                           <span class="align-middle"><?php echo _l('home_announcements'); ?></span>
                        </a>
                     </li>
                     <?php } ?>
                     <?php if(is_admin()){ ?>
                     <li class="nav-item">
                        <a class="nav-link " id="activity-tab" data-toggle="tab" href="#activity" aria-controls="activity" role="tab" aria-selected="true">
                           <i class="bx bx-window align-middle"></i>
                           <span class="align-middle"><?php echo _l('home_latest_activity'); ?></span>
                        </a>
                     </li>
                     <?php } ?>
                     </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="tasks" aria-labelledby="tasks-tab" role="tabpanel">
                        <a href="<?php echo admin_url('tasks/list_tasks'); ?>" class="mbot20 inline-block full-width"><?php echo _l('home_widget_view_all'); ?></a>
                        <div class="clearfix"></div>
                        <div class="_hidden_inputs _filters _tasks_filters">
                           <?php
                              echo form_hidden('my_tasks',true);
                              foreach($task_statuses as $status){
                                 $val = 'true';
                                 if($status['id'] == Tasks_model::STATUS_COMPLETE){
                                 $val = '';
                              }
                              echo form_hidden('task_status_'.$status['id'],$val);
                              }
                              ?>
                        </div>
                        <?php $this->load->view('admin/tasks/_table'); ?>
                     </div>
                     <?php if((get_option('access_tickets_to_none_staff_members') == 1 && !is_staff_member()) || is_staff_member()){ ?>
                     <div class="tab-pane" id="tickets" aria-labelledby="tickets-tab" role="tabpanel">
                        <a href="<?php echo admin_url('tickets'); ?>" class="mbot20 inline-block full-width"><?php echo _l('home_widget_view_all'); ?></a>
                           <div class="clearfix"></div>
                           <div class="_filters _hidden_inputs hidden tickets_filters">
                              <?php
                                 // On home only show on hold, open and in progress
                                 echo form_hidden('ticket_status_1',true);
                                 echo form_hidden('ticket_status_2',true);
                                 echo form_hidden('ticket_status_4',true);
                                 ?>
                           </div>
                           <?php echo AdminTicketsTableStructure(); ?>
                        </div>
                     <?php } ?>
                     <div class="tab-pane" id="projects" aria-labelledby="projects-tab" role="tabpanel">
                        <a href="<?php echo admin_url('projects'); ?>" class="mbot20 inline-block full-width"><?php echo _l('home_widget_view_all'); ?></a>
                           <div class="clearfix"></div>
                           <?php render_datatable(array(
                              _l('project_name'),
                              _l('project_start_date'),
                              _l('project_deadline'),
                              _l('project_status'),
                              ),'staff-projects',[], [
                              'data-last-order-identifier' => 'my-projects',
                              'data-default-order'  => get_table_last_order('my-projects'),
                              ]);
                              ?>
                     </div>
                     <div class="tab-pane" id="reminders" aria-labelledby="reminders-tab" role="tabpanel">
                        <a href="<?php echo admin_url('misc/reminders'); ?>" class="mbot20 inline-block full-width">
                           <?php echo _l('home_widget_view_all'); ?>
                        </a>
                        <?php render_datatable(array(
                           _l( 'reminder_related'),
                           _l('reminder_description'),
                           _l( 'reminder_date'),
                           ), 'my-reminders'); ?>
                     </div>
                     <?php if(is_staff_member()){ ?>
                     <div class="tab-pane" id="announcements" aria-labelledby="announcements-tab" role="tabpanel">
                     <?php if(is_admin()){ ?>
                                 <a href="<?php echo admin_url('announcements'); ?>" class="mbot20 inline-block full-width"><?php echo _l('home_widget_view_all'); ?></a>
                                 <div class="clearfix"></div>
                                 <?php } ?>
                                 <?php render_datatable(array(_l('announcement_name'),_l('announcement_date_list')),'announcements'); ?>
                     </div>
                     <?php } ?>
                     <?php if(is_admin()){ ?>
                     <div class="tab-pane" id="activity" aria-labelledby="activity-tab" role="tabpanel">
                     <a href="<?php echo admin_url('utilities/activity_log'); ?>" class="mbot20 inline-block full-width"><?php echo _l('home_widget_view_all'); ?></a>
                                 <div class="clearfix"></div>
                                 <div class="activity-feed">
                                    <?php foreach($activity_log as $log){ ?>
                                    <div class="feed-item">
                                       <div class="date">
                                          <span class="text-has-action" data-toggle="tooltip" data-title="<?php echo _dt($log['date']); ?>">
                                          <?php echo time_ago($log['date']); ?>
                                          </span>
                                       </div>
                                       <div class="text">
                                          <?php echo $log['staffid']; ?><br />
                                          <?php echo $log['description']; ?>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
                        <!-- Earnings Widget Swiper Ends -->