/*=========================================================================================
    File Name: nav.js
    Description: Navigation available in Bootstrap share general markup and styles, from the base .nav class to the active and disabled states. Swap modifier classes to switch between each style.
    ----------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
(function (window, document, $) {
  'use strict';
  $('.nav-tabs .nav-item .nav-link').click(function (e) {
    // alert('clicked e');
    // add current class to parent of active class
    $('.nav-tabs .nav-item .nav-link').removeClass('active');
    $(this).closest('.nav-link').addClass('active').closest('.nav-item').addClass('current').siblings().removeClass('current');
    // e.stopPropagation();
  });
})(window, document, jQuery);
